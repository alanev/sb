var gulp = require('gulp'),
	sync = require('run-sequence'),
	watch = require('gulp-watch');

// paths
var paths = require('./paths');

// task
var task = function () {
	
	watch([paths.modules + '**/*.htm', paths.src + '*.htm'], function () {
		sync('html', 'test:html', ['html2jade']);
	});
    
	watch([paths.modules + '**/*.{scss,css}', paths.src + '*.css'], function () {
		sync('css', 'test:css');
	});
    
	watch([paths.modules + '**/*.js', paths.src + '*.js'], function () {
		sync('js', 'test:js');
	});
	
	watch(paths.img.src, function () {
		gulp.start('img');
	});
	
	watch(paths.sprite.src, function () {
		gulp.start('sprite');
	});
	
	watch('config.json', function () {
		gulp.start('modules');
	})
};

// module
module.exports = task;