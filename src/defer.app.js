require('u-nl/u-nl');

var form = require('form/form');
form.init();

var article = require('article/article');
if (article.articles) {
    article.init();
}

var carousel = require('carousel/carousel');
if (carousel.carousels) {
    carousel.init();
}

var search = require('search/search');
search.init();

var menu = require('menu/menu');
menu.init();