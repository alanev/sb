var html2jade = require('html2jade');
var glob = require('glob');
var fs = require('fs');

module.exports = function () {
    glob('build/*.htm', function (err, list) {
        list.forEach(filePath => {
            fs.readFile(filePath, function (err, file) {
                html2jade.convertHtml(file.toString(), {}, function (err, jade) {
                    fs.writeFile(filePath.replace('build/', 'jade/').replace('.htm', '.jade'), jade, function (err) {
                        console.log('done', filePath);
                    });
                })
            });
        });
    });
}