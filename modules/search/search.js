var search = {
    icon: document.querySelector('.search__toggle'),
    element: document.querySelector('.search'),
    toggle () {
        this.icon.classList.toggle('_active');
        this.element.classList.toggle('_active');
    },
    init () {
        var that = this;
        this.icon.addEventListener('click', function () {
            that.toggle();
        })
    }
};
module.exports = search;