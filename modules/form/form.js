var form = {
    inputs: document.querySelectorAll('input'),
    test (input) {
        var parent = input.parentNode.classList,
            isParent = parent.contains('form__field');
        
        if (input.value != '') {
            input.classList.add('_dirty');
            if (isParent) parent.add('_dirty');
        } else {
            input.classList.remove('_dirty');
            if (isParent) parent.remove('_dirty');
        }
    },
    init () {
        var that = this;
        
        this.inputs.forEach(function (input) {
            that.test(input);
            input.addEventListener('change', function () {
                that.test(input);
            });
        });
    }
}
module.exports = form;