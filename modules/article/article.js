var article = {
    articles: document.querySelectorAll('.article__more'),
    init () {
        this.articles.forEach(function (item) {
            item.addEventListener('click', function (e) {
                e.target.classList.toggle('_active');
            });
        });
    }
};
module.exports = article;