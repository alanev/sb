var carousel = {
    carousels: document.querySelectorAll('.carousel').map(function (item) {
        return {
            carousel: item,
            items: item.querySelectorAll('.carousel__item'),
            active: 0
        };
    }),
    move (dir, parent) {
        var items = parent.items;
        
        items.item(parent.active).classList.remove('_active');
        
        if (dir === 'next') {
            parent.active++;
        } else if (dir === 'prev') {
            parent.active--;
        }
        
        if (parent.active >= items.length) {
            parent.active = 0;
        }
        if (parent.active < 0) {
            parent.active = items.length - 1;
        }
        
        items.item(parent.active).classList.add('_active');
    },
    init () {
        var that = this;
        this.carousels.forEach((item) => {
            item.carousel.querySelector('.carousel__arrow._prev').addEventListener('click', function () {
                that.move('prev', item)
            });
            item.carousel.querySelector('.carousel__arrow._next').addEventListener('click', function () {
                that.move('next', item)
            });
        });
    }
};
module.exports = carousel;